library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library work;
use work.hash_pkg.all;

entity cuckoo_tb is

  port (

         hit          : out std_logic; -- 1 if query value is in stash
         search_value : out std_logic_vector(VALUE_LEN -1  downto 0); -- value of the key quered
         not_empty    : out std_logic
         --key_out      : out std_logic_vector(KEY_LEN -1 downto 0)

       );

end cuckoo_tb;

architecture behav of cuckoo_tb is

  signal clk    : std_logic := '0';
  signal reset  : std_logic := '1';
  signal enable : std_logic := '0';

  signal insert       : std_logic := '0';
  signal search       : std_logic := '0';
  signal remove       : std_logic := '0';
  signal update       : std_logic := '0';
  signal ready        : std_logic;
  signal key          : std_logic_vector( KEY_LEN -1 downto 0) := (others => '0');
  signal search_key   : std_logic_vector( KEY_LEN -1 downto 0) := (others => '0');
  signal value        : std_logic_vector( VALUE_LEN -1 downto 0) := (others => '0');
  signal key_out      : std_logic_vector( KEY_LEN -1 downto 0) := (others => '0');
  signal value_out    : std_logic_vector( VALUE_LEN -1 downto 0) := (others => '0');

begin

  CUCKOO_I: entity work.cuckoo port map (  
  
                                         clock => clk,
                                         reset => reset,
                                         insert => insert,
                                         search => search,
                                         remove => remove,
                                         update => update,
                                         key => key,
                                         search_key => search_key,
                                         value => value,
                                        
                                         hit => hit,
                                         search_value => search_value

                                       );

  clk <= not(clk) after 10ns;

  reset <= '0' after 40ns;

  enable <= '1' after 40ns;

  insert <= '1' after 90ns, '0' after 110ns; -- '1' after 130ns, '0' after 150ns;
  key <= (others => '1') after 70ns; -- x"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" after 130ns;
  value <= x"01234567" after 70ns;-- x"aabbccdd" after 130ns;

  --search_key <=  (others => '1') after 70ns;
  --update <= '1' after 130ns;
  --search <= '1' after 130ns, '0' after 150ns;
  --remove <= '1';

end behav;
